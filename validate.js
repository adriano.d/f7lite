const Joi = require('@hapi/joi')

// contact form validation
const contactFormValidation = data => {
    const schema = Joi.object ({
        date: Joi.date(),
        name: Joi.string()
            .min(2)
            .required(),
        email: Joi.string()
            .min(6)
            .required()
            .email(),
        message: Joi.string()
            .required()
    })
    return schema.validate(data)
}

module.exports.contactFormValidation = contactFormValidation
