require('dotenv').config()
const nodemailer = require('nodemailer')

const sendEmail = async req => {
    // setup transporter
    let transporter = nodemailer.createTransport({
        host: 'smtp-mail.outlook.com',
        port: 587,
        tls: {
            ciphers:'SSLv3',
            // rejectUnauthorized: false
         },
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.OUTLOOK_EMAIL, 
            pass: process.env.OUTLOOK_PASSWORD
        }
    })

    // send mail with defined transport object
    await transporter.sendMail({
        from: process.env.OUTLOOK_EMAIL,  
        to: process.env.OUTLOOK_EMAIL, 
        subject: `New Message from ${req.body.name} at ${req.body.email}`, 
        text: req.body.message, 
    })
}

module.exports.sendEmail = sendEmail
