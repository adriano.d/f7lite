require('dotenv').config()
const express = require('express')
const app = express()
const path = require('path')
const logger = require('morgan')
const hbs = require('hbs')
const indexRouter = require('./routes/index')
const mongoose = require('mongoose')

//create default connection to database and bind to error event
mongoose.connect(process.env.DB_LINK, {useUnifiedTopology: true, useNewUrlParser: true})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

// handlebars helpers
hbs.registerHelper('dateFormat', require('handlebars-dateformat'))

// app middleware
app.use(logger('dev'));
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

// route handler
app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
