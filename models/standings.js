const mongoose = require('mongoose')
const Schema = mongoose.Schema

const StandingsSchema = new Schema(
    {
        position: {type: Number, required: true},
        team: {type: String, required: true},
        gp: {type: Number, required: true},
        wins: {type: Number, required: true},
        loss: {type: Number, required: true},
        draw: {type: Number, required: true},
        gf: {type: Number, required: true},
        ga: {type: Number, required: true},
        pm: {type: Number, required: true},
        pts: {type: Number, required: true},
     }
)

module.exports = mongoose.model('Standings', StandingsSchema)