const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ScheduleSchema = new Schema(
    {
        date: {type: Date,required: true},
        time: {type: String, required: true},   
        home: {type: String, required: true},
        away: {type: String, required: true},
        field: {type: String, required: true},
     }
)

module.exports = mongoose.model('Schedule', ScheduleSchema)