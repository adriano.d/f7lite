require('dotenv').config()
const footySevens = require('./footy-sevens');
const Standings = require('./models/standings');
const Schedule = require('./models/schedule');

const uploadToDB = async () => {
    await footySevens.initialize()
    await footySevens.login(process.env.USER, process.env.PASSWORD)

    // TODO: handle possibility of page elements not found if footysevens changes their format
    standingsData = await footySevens.standings(process.env.STANDINGS_LINK)
    scheduleData = await footySevens.schedule(process.env.SCHEDULE_LINK)
    console.log(standingsData)
    await footySevens.close()

    standingsData.forEach((element, index) => {
        Standings.updateOne({team: element.team}, 
            {
                position: element.pos,
                team: element.team,
                gp: element.gp,
                wins: element.wins,
                loss: element.loss,
                draw: element.draw,
                gf: element.gf,
                ga: element.ga,
                pm: element.pm,
                pts: element.pts
            },
            {
                upsert: true
            })
        .then(() => {
            if (index == standingsData.length-1) {
                db.close()
            }
        })
     })
     
     scheduleData.forEach((element, index) => {
         Schedule.updateOne({date: element.date}, 
            {
                date: element.date,
                time: element.time,
                home: element.home,
                away: element.away,
                field: element.field
            },
            {
                upsert: true
            })
        .then(() => {
            if (index == scheduleData.length-1) {
                db.close()
            }
        })
    })
}

// update database every 6 hours
// setInterval(() => uploadToDB(), ((1000 * 60 * 60 * 24)/4))

uploadToDB()







