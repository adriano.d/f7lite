require('dotenv').config()
const puppeteer = require('puppeteer');

const footySevens = {
  browser: null,
  page: null,

  // initlialize connection to browser and homepage of footy sevens website
  initialize: async () => {
    footySevens.browser = await puppeteer.launch()
    footySevens.page = await footySevens.browser.newPage()
  },

  // login to footysevens, puts my own personal username and password into their login form
  login: async (username, password) => {
    await footySevens.page.goto(process.env.LOGIN_URL)
    await footySevens.page.type('input[placeholder="Username"]', username)
    await footySevens.page.type('input[placeholder="Password"]', password) 
    await footySevens.page.click('button.ui-button')
  },
 
  // gets standings page data (pts, wins, losses, gf, ga, etc.)
  standings: async (standingsLink) => {
      await footySevens.page.goto(standingsLink)
      
      // evaluate page and store standings table data in an array
      const fullStandings = await footySevens.page.evaluate(() => {
        const tableRows = Array.from(document.querySelectorAll('.generalDataTable tr.ui-widget-content'))    
        const data = []  
        let pos = 1

        // iterates over array and stores data in appropriate fields in a new array
        for (let i = 0; i < 8; i++) {
          data.push({
            pos: pos++,
            team: tableRows[i].querySelector('td.team').innerText.trim().toLowerCase(),
            gp: tableRows[i].querySelector('tr td:nth-child(2)').innerText.trim(),
            wins: tableRows[i].querySelector('tr td:nth-child(3)').innerText.trim(),
            loss: tableRows[i].querySelector('tr td:nth-child(4)').innerText.trim(),
            draw: tableRows[i].querySelector('tr td:nth-child(5)').innerText.trim(),
            gf: tableRows[i].querySelector('tr td:nth-child(6)').innerText.trim(),
            ga: tableRows[i].querySelector('tr td:nth-child(7)').innerText.trim(),
            pm: tableRows[i].querySelector('tr td:nth-child(8)').innerText.trim(),
            pts: tableRows[i].querySelector('tr td:nth-child(9)').innerText.trim(),
          })
        }
        return data
      })
      return fullStandings 
  },

  // similar process to what happens in the standings function above, but for the schedule
  schedule: async (scheduleLink) => {
      
      await footySevens.page.goto(scheduleLink)
      const schedule = await footySevens.page.evaluate(() => {

        const dateRows = Array.from(document.querySelectorAll('.ui-datatable-subtable-header'))
        const dataRows = Array.from(document.querySelectorAll('tr.ui-widget-content'))
        const data = []

        for (let i = 0; i < dateRows.length; i++) {
          data.push({
            date: dateRows[i].innerText.trim(),
            time: dataRows[i].querySelector('.startTime').innerText.trim().toLowerCase(),
            home: dataRows[i].querySelector('.homeTeam').innerText.trim().toLowerCase(),
            away: dataRows[i].querySelector('.awayTeam').innerText.trim().toLowerCase(),
            field: dataRows[i].querySelector('.location').innerText.trim().toLowerCase(),
          })
        }
         return data 
      })
       return schedule
  },

  // closes puppeteer browser
  close: async() => {
    
    await footySevens.browser.close()
  },
}

module.exports = footySevens;

