const Schedule = require('../models/schedule')

exports.schedule_table = (req, res, next) => {
    Schedule.find()
    .sort({ date: 1})
    .exec((err, schedule_table) => {
        if(err) { return next(err); }

        //successful, so render
        res.render('schedule', { title: 'Schedule Table', schedule: schedule_table});
    })
}