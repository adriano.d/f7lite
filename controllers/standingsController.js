const Standings = require('../models/standings')

exports.standings_table = (req, res, next) => {
    Standings.find()
    .sort({ division: 1, position: 1})
    .exec((err, standings_table) => {
        if(err) { return next(err) }

        //successful, so render
        res.render('standings', { title: 'Standings Table', standings: standings_table});
    })
}




