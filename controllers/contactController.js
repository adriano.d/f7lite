const Contact = require('../models/contact')
const {contactFormValidation} = require('../validate') 
const {sendEmail} = require('../node-mailer')

exports.contact_page = (req, res) => {
    res.render('contact', {title: 'Contact'})
}

exports.add_contact_post = async (req, res) => {

    // validatate contact form data submitted
    const {error} = contactFormValidation(req.body)

    // TOOD: redirect error to proper error page
    if(error) return res.status(400).render('contact', {error})

    // create new contact post
    const contact = new Contact({
        date: new Date(),
        name: req.body.name,
        email: req.body.email,
        message: req.body.message,
    })

    // try saving new message to database, and emailing a copy to footysevens admin
    try {
        await contact.save()
        res.render('contact', {success: 'thanks for messaging me bud'})
        await sendEmail(req).catch(console.error)
    } catch(err) {
        // TODO: set up route upon unsuccessful addition of contact form comment 
        res.render('contact', {failure: 'something went wrong there'})
    }
}