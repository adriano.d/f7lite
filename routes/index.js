const express = require('express');
const router = express.Router();

// route controllers
const standings_controller = require('../controllers/standingsController')
const schedule_controller = require('../controllers/scheduleController')
const about_controller = require('../controllers/aboutController')
const contact_controller = require('../controllers/contactController')

// redirect '/' path to standings page
router.get('/', (req, res) => {res.redirect('/standings') })

// get request handlers
router.get('/standings', standings_controller.standings_table)
router.get('/schedule', schedule_controller.schedule_table)
router.get('/about', about_controller.about_page)
router.get('/contact', contact_controller.contact_page)

// post requests
router.post('/contact', contact_controller.add_contact_post)

module.exports = router;
