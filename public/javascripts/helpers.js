// this helper exists because of what the footysevens admins named the individual sign-ups team
const renameIndividualTeam = selector => {
    document.querySelectorAll(selector).forEach(e => {
        if(e.innerHTML == 'tuesday summer mens @ immaculata - b (sept 3 start)') e.innerHTML = 'individuals'
    })  
}

// getting yesterdays date 
const today = new Date();
today.setDate(today.getDate() - 1)
const yesterday = Date.parse(today)

let scheduleRows = document.querySelectorAll('tr.schedule-row')

const gameDayOnSchedule = Array.from(document.querySelectorAll('td.date-unformatted'))
                               .map(e => Date.parse(e.innerHTML))

// this function 'greys out' a row on the schedule table if the game has already passed                               
const compareDates = () => {
    gameDayOnSchedule.forEach((e, i) => {
        if(e < yesterday) scheduleRows[i].style.color = '#a1aac4'
    })
}

compareDates()
renameIndividualTeam('.team')
renameIndividualTeam('.home-team')
renameIndividualTeam('.away-team')

document.querySelector('.submit-wrapper').addEventListener('click', () => {
    document.forms[0].submit()
})